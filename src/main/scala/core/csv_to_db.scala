package core

import org.mongodb.scala._
import org.mongodb.scala.model.Aggregates._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Success, Failure}
import scala.concurrent.Future

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object CSVtoDB {

  val mongoClient: MongoClient = MongoClient()
  val database: MongoDatabase = mongoClient.getDatabase("mydb")
  val files = List("airports", "runways", "countries")
  val list_collections: List[(MongoCollection[Document], String)] = files.map((file) => {
    (database.getCollection(file), file.concat(".csv"))
  })

  def drop_all() : Future[Completed] = {
    val f = database.drop().toFuture()
    f.onComplete {
      case Success(_) => println(s"mydb dropped")
      case Failure(t) => println("An error has occurred: " + t.getMessage)
    }
    f
  }

  def insert_all() : Future[List[Completed]] = {
    val test = list_collections.map ((collection) => {
      println(s"Begin to load ${collection._2}")
      val path = "csv/" // path to csv files
      val bufferedSource = scala.io.Source.fromFile(path.concat(collection._2))
      val tmp = bufferedSource.getLines.find(_ => true)
      val first = tmp.getOrElse("")
        .toString.split(",", -1)
        .map(_.replaceAll("\"", ""))
      val lst = bufferedSource.getLines.map(
        (line) => {
          val cols = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1)
                         .map(_.replaceAll("\"", ""))
          val doc: Document = (0 to (cols.length - 1))
            .filter((n) => (!cols(n).isEmpty))
            .foldLeft(Document()) (
                (acc, n) => {
                  acc ++ Document(first(n) -> cols(n))
                })
          doc
        })
      val f = collection._1.insertMany(lst.toIndexedSeq).toFuture()
      f.onComplete {
        case Success(_) => println(s"${collection._2} loaded")
        case Failure(t) => println("An error has occurred: " + t.getMessage)
      }
      bufferedSource.close
      f
    })
    Future.sequence(test)
  }

  def create_indexes() : Future[List[String]] = {
    val f1 = list_collections(0)._1.createIndex(Document("id" -> 1)).toFuture()
    val f2 = list_collections(1)._1.createIndex(Document("airport_ref" -> 1)).toFuture()
    val f3 = list_collections(2)._1.createIndex(Document("code" -> 1)).toFuture()
    val f4 = list_collections(2)._1.createIndex(Document("name" -> 1)).toFuture()
    val f = for {
      ob1 <- f1
      ob2 <- f2
      ob3 <- f3
      ob4 <- f4
    } yield (List(ob1, ob2, ob3, ob4))

    f.onComplete {
      case Success(_) => println(s"Indexes created")
      case Failure(t) => println("An error has occurred: " + t.getMessage)
    }
    f
  }

  def create_links() : Future[Seq[Document]] = {
    val f = list_collections(0)._1.aggregate(
      Seq(
        lookup("runways", "id", "airport_ref", "runways"),
        out("links")
      ))
      .toFuture()
    f.onComplete {
      case Success(_) => println(s"Collections linked")
      case Failure(t) => println("An error has occurred: " + t.getMessage)
    }
    f
  }


  def run() : Future[Seq[Document]] = {
    val future_drop = drop_all()
    val f_all = future_drop.flatMap (
      _ => insert_all()
    ).flatMap (
      _ => create_indexes()
    ).flatMap (
      _ => create_links()
    )

    f_all.onComplete {
      case Success(_) => mongoClient.close
      case Failure(t) => println("An error has occurred: " + t.getMessage)
    }

    f_all
  }

  /*def main(args: Array[String]): Unit = {

    val f_all = run()
    Await.ready(f_all, Duration.Inf)
    mongoClient.close
  }*/
}



