package core

import org.mongodb.scala._
import org.scalatest._

import scala.concurrent.Future
import scala.util.{Success, Failure}
import org.scalatest.Assertions._
import scala.concurrent.Await
import scala.concurrent.duration._


class ConnectionSpec extends FunSuite with Matchers {
  test("Process query France") {
    val f = Connection.process_query(List("France"))
    Await.ready(f, 1.seconds)
    val test = f.value match {
      case Some(Success(x)) => true
      case _ => false
    }
    assert(test)
  }

  test("Process query fr") {
    val f = Connection.process_query(List("fr"))
    Await.ready(f, 1.seconds)
    val test = f.value match {
      case Some(Success(x)) => true
      case _ => false
    }
    assert(test)
  }

  test("Process query fra") {
    val f = Connection.process_query(List("fra"))
    Await.ready(f, 1.seconds)
    val test = f.value match {
      case Some(Success(x)) => true
      case _ => false
    }
    assert(test)
  }

  test("Process query fsdf") {
    val f = Connection.process_query(List("fsdf"))
    Await.ready(f, 1.seconds)
    val test = f.value match {
      case Some(Success(x)) => true
      case _ => false
    }
    assert(test)
  }

  test("Process report : Runways types for each country") {
    val f = Connection.display_runways_type
    Await.ready(f, 1.seconds)
    val test = f.value match {
      case Some(Success(x)) => true
      case _ => false
    }
    assert(test)
  }

  test("Process report : 10 countries with highest/lowest number of airports") {
    val f = Connection.display_country_air
    Await.ready(f, 1.seconds)
    val test = f.value match {
      case Some(Success(x)) => true
      case _ => false
    }
    test should be (true)
  }

  test("Process report : top 10 most common runway latitude") {
    val f = Connection.display_common_runways
    Await.ready(f, 1.seconds)
    val test = f.value match {
      case Some(Success(x)) => true
      case _ => false
    }
    assert(test)
  }
}
