package core

import org.mongodb.scala._
import org.mongodb.scala.model.Aggregates._
import org.mongodb.scala.model.Accumulators._
import org.mongodb.scala.model.Sorts._
import org.mongodb.scala.model.Projections._
import org.mongodb.scala.model.Filters._

import org.mongodb.scala.Document

import scala.collection.JavaConversions._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Success, Failure}
import scala.concurrent.Future


object Connection {
  val client : MongoClient = MongoClient()
  val database : MongoDatabase = client.getDatabase("mydb")

  val airports : MongoCollection[Document]
      = database.getCollection("airports")
  val runways : MongoCollection[Document]
      = database.getCollection("runways")
  val countries : MongoCollection[Document]
      = database.getCollection("countries")
  val links : MongoCollection[Document]
      = database.getCollection("links")

  def read_input : String = {
    def aux : String = Option(scala.io.StdIn.readLine()) match
    {
      case None => aux
      case Some("") => read_input
      case Some(s) => s
    }
    aux
  }

  def process_query(args : List[String]) : Future[Any] = {
    val s = args.reduce((a, b) => a + " " + b)

    def find_by_country(input : String) : Future[Any] = {
      countries.find(Document("name" -> Document("$regex" -> ("^"+input), "$options" -> "i")))
        .toFuture()
        .flatMap(
          (results) => {
            val list_f = results.map(
              res => find_by_code(res("code").asString.getValue)
            )
            if (list_f.length == 0)
              Future { println("Empty Result") }
            else
              Future.sequence(list_f)
          }
        )
    }

    def find_by_code(input : String) : Future[Any] = {
      links.find(Document("iso_country" -> input.toUpperCase()))
        .toFuture()
        .flatMap(
          (results) => {
            val list_f = results.map(
              res => Future {
                println(
                  res("runways").asArray.toList.foldLeft(s"${res("name").asString.getValue} :")(
                    (acc,bson) => {
                      val doc = bson.asDocument
                      acc + s"\n\t- runway_id : ${doc("id").asString.getValue}," +
                        s" length : ${doc.get("length_ft") match {
                          case null => "UNKNOWN"
                          case x => x.asString.getValue
                        }}," +
                        s" width : ${doc.get("width_ft") match {
                          case null => "UNKNOWN"
                          case x => x.asString.getValue
                        }}," +
                        s" surface : ${doc.get("surface") match {
                          case null => "UNKNOWN"
                          case x => x.asString.getValue
                        }}"
                    }
                  )
                )
              }
            )
            if (list_f.length == 0)
                find_by_country(input)
            else
              Future.sequence(list_f)
          }
        )
    }

    val f = find_by_code(s)
    f.onComplete {
      case Success(_) => print_request
      case Failure(t) => println("An error has occurred: " + t.getMessage)
    }
    f

  }



  def display_runways_type : Future[Any] = {
    println("\nRunways types for each country :")
    val f = links.aggregate(Seq(
      unwind("$runways"),
      group("$iso_country", addToSet("type", "$runways.surface")),
      lookup("countries", "_id", "code", "country"),
      unwind("$country")
    ))
      .toFuture()
      .flatMap(
        (results) => {
          val list_f = results.map(
            res => Future {
              println(
                "\t" + res("country").asDocument.get("name").asString.getValue + " : " +
                res("type").asArray
                  .toList
                  .foldLeft("")(
                    (acc, bsonval) => acc + bsonval.asString.getValue + " | "
                  )
              )
            }
          )
          if (list_f.length == 0)
            Future { println("Empty Result") }
          else
            Future.sequence(list_f)
        }
      )
    f.onComplete {
      case Success(_) => print_reports
      case Failure(t) => println("An error has occurred: " + t.getMessage)
    }
    f
  }

  def display_common_runways : Future[Unit] = {
    val f = runways.aggregate(Seq(group("$le_ident", sum("count", 1)),
                          sort(orderBy(descending("count"))),
                          limit(10)))
           .toFuture()
           .map (
              (results) => println(
                results.foldLeft("\nThe top 10 most common runway latitude :")(
                  (acc,res) => acc + s"\n\t${res("_id").asString.getValue} : ${res("count").asInt32.getValue}"
                )
              )
           )
    f.onComplete {
      case Success(_) => print_reports
      case Failure(t) => println("An error has occurred: " + t.getMessage)
    }
    f
  }

  def display_country_air : Future[(Seq[Document], Seq[Document])] = {
    val f1 = airports.aggregate(Seq(
      group("$iso_country", sum("count", 1)),
      sort(orderBy(descending("count"))),
      limit(10),
      lookup("countries", "_id", "code", "country"),
      unwind("$country")
    )).toFuture()

    val f2 = airports.aggregate(Seq(
      group("$iso_country", sum("count", 1)),
      sort(orderBy(ascending("count"))),
      limit(10),
      lookup("countries", "_id", "code", "country"),
      unwind("$country")
    )).toFuture()

    val result = for {
      ob1 <- f1
      ob2 <- f2
    } yield (ob1, ob2)

    def nb_airports(res : Document) : String = {
      "\t" + res("country").asDocument.get("name").asString.getValue + " : " + res("count").asInt32.getValue + "\n"
    }

    result.onComplete {
      case Success(r) => {
        println(
          r._1.foldLeft("\n10 countries with highest number of airports :\n")(
            (acc, elt) => acc + nb_airports(elt)
          )
        )
        println(
          r._2.foldLeft("\n10 countries with lowest number of airports :\n")(
            (acc, elt) => acc + nb_airports(elt)
          )
        )
        print_reports
      }
      case Failure(t) => println("An error has occurred: " + t.getMessage)
    }
    result
  }

  def print_reports : Unit = {
    println("\nWhat report do you want (1/2/3/quit)? \n"
      + "\t1. 10 countries with highest/lowest number of airports\n"
      + "\t2. Type of runways per country\n"
      + "\t3. The top 10 most common runway latitude")
  }

  def process_reports : Unit = {
    print_reports
    def ask_user : Unit = read_input.split(' ').toList match
    {
      case "1" :: Nil =>
      {
        display_country_air
        ask_user
      }
      case "2" :: Nil =>
      {
        display_runways_type
        ask_user
      }
      case "3" :: Nil =>
      {
        display_common_runways
        ask_user
      }
      case "quit" :: Nil => print_request
      case _ =>
      {
        println("- Input not understood.")
        ask_user
      }
    }

    ask_user
  }

  def print_request : Unit = {
    println ("\n- What is your request (query <country>/reports/quit)? Type 'help' for more information.")
  }

  def request : Unit = {
    print_request
    def ask_user : Unit = read_input.split(' ').toList match {
      case "help" :: Nil => {
        println("\t'query [country name or code]' to display "
          + "all airports of the specified country")
        println("\t'reports' to display:\n"
          + "\t\t• 10 countries with highest number of airports (with "
          + "count) and countries with lowest number of airports.\n"
          + "\t\t• Type of runways per country\n"
          + "\t\t• The top 10 most common runway latitude")
        println("\t'quit' to leave the command line interpreter\n")
        ask_user
      }
      case "query" :: tail if tail != Nil => {
        process_query(tail)
        ask_user
      }
      case "reports" :: Nil => {
        process_reports
        ask_user
      }
      case "quit" :: Nil => ()
      case _ => {
        println("- Could not understand request")
        ask_user
      }
    }
    // code execution
    ask_user
  }

  def main(args : Array[String]) = {
    val f_all = CSVtoDB.run()
    f_all.onComplete {
      case Success(_) => print_request
      case Failure(t) => println("An error has occurred: " + t.getMessage)
    }
    request
    client.close
  }
}
