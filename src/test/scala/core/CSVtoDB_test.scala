package core

import org.mongodb.scala._
import org.scalatest._

import scala.concurrent.Future
import scala.util.{Success, Failure}
import org.scalatest.Assertions._
import scala.concurrent.Await
import scala.concurrent.duration._


class CSVtoDBSpec extends FunSuite with Matchers {
  test("Insert all data from CSV files to collections in mydb") {
    val f = CSVtoDB.insert_all
    Await.ready(f, 5.seconds)
    val test = f.value match {
      case Some(Success(x)) => true
      case _ => false
    }
    assert(test)
  }

  test("Create Indexes in mydb collections") {
    val f = CSVtoDB.create_indexes
    Await.ready(f, 5.seconds)
    val test = f.value match {
      case Some(Success(x)) => true
      case _ => false
    }
    assert(test)
  }

  test("Create aggregate collection between airports and runways in mydb") {
    val f = CSVtoDB.create_links
    Await.ready(f, 10.seconds)
    val test = f.value match {
      case Some(Success(x)) => true
      case _ => false
    }
    assert(test)
  }

  test("run all the functions in CSVtoDB") {
    val f = CSVtoDB.run
    Await.ready(f, 15.seconds)
    val test = f.value match {
      case Some(Success(x)) => true
      case _ => false
    }
    assert(test)
  }

  test("Drop all data in mydb") {
    val f = CSVtoDB.drop_all
    Await.ready(f, 5.seconds)
    val test = f.value match {
      case Some(Success(x)) => true
      case _ => false
    }
    assert(test)
  }
}

